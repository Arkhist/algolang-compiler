%{
#include "../compiler.hpp"

#define YYDEBUG 1

int yylex();
void yyerror(const char *s);

extern int yylineno;

Compiler* currentParser = 0;
%}

%code requires {
    #include "../types.hpp"

    #include "../ast.hpp"
    #include "../statement.hpp"

    #include <iostream>
    #include <cstring>
}

%define parse.error verbose

%union {
  bool boolVal;
  int intVal;
  double realVal;
  char* strVal;
  char chrVal;

  StatementList* statementList;

  Statement* statement;
  Expression* expression;

  AlgorithmAST* ast;
  VariableList* varList;

  std::vector<std::string>* strList;
  std::vector<Expression*>* exprList;
};


%token <intVal> NUMBER
%token <realVal> FLOATNUMBER
%token <chrVal> CHARACTER
%token <strVal> STRING

%token <strVal> IDENTIFIER

%token <boolVal> ALGO_TRUE
%token <boolVal> ALGO_FALSE

%token END

%token ALGO_DECL ALGO_ROLE ALGO_VAR ALGO_BEGIN ALGO_END
%token <strVal> ALGO_ROLE_CONTENT

%token ALGO_IF ALGO_ELSE ALGO_THEN ALGO_ENDIF
%token ALGO_FOR ALGO_OF ALGO_TO ALGO_DO ALGO_ENDFOR
%token ALGO_STEP0 ALGO_STEP1 ALGO_STEP_SHORT
%token ALGO_WHILE0 ALGO_WHILE1 ALGO_WHILE_SHORT
%token ALGO_ENDWHILE

%token COLON COMMA

%token ASSIGNMENT_OP
%token <intVal> CONDITIONAL_OP
%token PLUS MINUS TIMES DIVIDE MODULO
%token OR AND NOT

%token LEFT_PAREN RIGHT_PAREN

%token NEW_LINE

%token LEX_ERROR

%start Program

%type <strVal> Identifier
%type <statementList> StatementList

%type <statement> Statement
%type <statement> IfStatement
%type <statement> LoopStatement

%type <ast> Algorithm
%type <strVal> AlgoName
%type <strVal> AlgoDesc

%type <varList> AlgoVars
%type <varList> VarDeclList
%type <strList> IdList

%type <exprList> ArgExprList

%type <expression> AssiExpression
%type <expression> CondExpression
%type <expression> OrExpression
%type <expression> AndExpression
%type <expression> NotExpression
%type <expression> EqExpression
%type <expression> PostExpression
%type <expression> MulExpression
%type <expression> AddExpression
%type <expression> PrimExpression
%%

Whitespace      : NewLine | ;
NewLine         : NEW_LINE | NewLine NEW_LINE;

Program         : Program Whitespace Algorithm {
                    if(!currentParser->addAlgorithm($3))
                        YYABORT;
                }
                | Program Whitespace END {
                    YYACCEPT;
                }
                | Whitespace Algorithm {
                    if(!currentParser->addAlgorithm($2))
                        YYABORT;
                }
                | Whitespace END {

                };

Algorithm       : AlgoName AlgoDesc AlgoVars ALGO_BEGIN NewLine StatementList ALGO_END {
                    $$ = new AlgorithmAST($1, $2, $3, $6);
                }
                | AlgoName AlgoDesc AlgoVars ALGO_BEGIN NewLine ALGO_END {
                    $$ = new AlgorithmAST($1, $2, $3, nullptr);
                };
AlgoName        : ALGO_DECL Identifier NewLine {
                    $$ = $2;
                }
                | ALGO_DECL NewLine {
                    $$ = strdup("");
                };
AlgoDesc        : ALGO_ROLE COLON ALGO_ROLE_CONTENT NewLine {
                    $$ = $3;
                }
                | {
                    $$ = strdup("");
                };
AlgoVars        : ALGO_VAR NewLine VarDeclList {
                    $$ = $3;
                }
                | ALGO_VAR NewLine {
                    $$ = NULL;
                }
                | {
                    $$ = new VariableList();
                }
                ;
VarDeclList     : VarDeclList IdList COLON Identifier NewLine {
                    $$ = $1;
                    $$->appendVariables($2, $4);
                    delete $2;
                }
                | IdList COLON Identifier NewLine {
                    $$ = new VariableList();
                    $$->appendVariables($1, $3);
                    delete $1;
                };
IdList          : IdList COMMA Identifier {
                    $$ = $1;
                    $$->push_back($3);
                }
                | Identifier {
                    $$ = new std::vector<std::string>();
                    $$->push_back($1);
                };
StatementList   : StatementList Statement {
                    $$ = $1;
                    $$->appendStatement($2);
                }
                | Statement {
                    $$ = new StatementList();
                    $$->appendStatement($1);
                };
Statement       : AssiExpression NewLine {
                    $$ = (Statement*)$1;
                }
                | IfStatement {
                    $$ = (Statement*)$1;
                }
                | LoopStatement {
                    $$ = (Statement*)$1;
                };
AssiExpression  : CondExpression {
                    $$ = $1;
                }
                | Identifier ASSIGNMENT_OP CondExpression {
                    $$ = new AssignmentExpression($1, $3);
                    $$->lineno = yylineno;
                };
Identifier      : IDENTIFIER {
                    $$ = $1;
                }
                | ALGO_TO {
                    $$ = strdup("a");
                };
PrimExpression  : Identifier {
                    $$ = new ConstantExpression($1, true);
                    $$->lineno = yylineno;
                }
                | STRING {
                    $$ = new ConstantExpression($1, false);
                    $$->lineno = yylineno;
                }
                | FLOATNUMBER {
                    $$ = new ConstantExpression($1);
                    $$->lineno = yylineno;
                }
                | NUMBER {
                    $$ = new ConstantExpression($1);
                    $$->lineno = yylineno;
                }
                | CHARACTER {
                    $$ = new ConstantExpression($1);
                    $$->lineno = yylineno;
                }
                | ALGO_TRUE {
                    $$ = new ConstantExpression($1);
                    $$->lineno = yylineno;
                }
                | ALGO_FALSE {
                    $$ = new ConstantExpression($1);
                    $$->lineno = yylineno;
                }
                | LEFT_PAREN AssiExpression RIGHT_PAREN {
                    $$ = $2;
                };
CondExpression  : OrExpression {
                    $$ = $1;
                };
OrExpression    : AndExpression {
                    $$ = $1;
                }
                | OrExpression OR AndExpression {
                    $$ = new LogicalExpression($1, $3, LogicalExpression::Operation::OR);
                    $$->lineno = yylineno;
                };
AndExpression   : EqExpression {
                    $$ = $1;
                }
                | AndExpression AND EqExpression {
                    $$ = new LogicalExpression($1, $3, LogicalExpression::Operation::AND);
                    $$->lineno = yylineno;
                };
EqExpression    : NotExpression {
                    $$ = $1;
                }
                | EqExpression CONDITIONAL_OP NotExpression {
                    $$ = new LogicalExpression($1, $3, (LogicalExpression::Operation)$2);
                    $$->lineno = yylineno;
                };
NotExpression   : AddExpression {
                    $$ = $1;
                }
                | NOT NotExpression {
                    $$ = new LogicalExpression($2, NULL, LogicalExpression::Operation::NOT);
                    $$->lineno = yylineno;
                };
AddExpression   : MulExpression {
                    $$ = $1;
                }
                | AddExpression PLUS MulExpression {
                    $$ = new ArithmeticExpression($1, $3, ArithmeticExpression::Operation::PLUS);
                    $$->lineno = yylineno;
                }
                | AddExpression MINUS MulExpression {
                    $$ = new ArithmeticExpression($1, $3, ArithmeticExpression::Operation::MINUS);
                    $$->lineno = yylineno;
                };
MulExpression   : PostExpression {
                    $$ = $1;
                }
                | MulExpression TIMES PostExpression {
                    $$ = new ArithmeticExpression($1, $3, ArithmeticExpression::Operation::TIMES);
                    $$->lineno = yylineno;
                }
                | MulExpression DIVIDE PostExpression {
                    $$ = new ArithmeticExpression($1, $3, ArithmeticExpression::Operation::DIVIDE);
                    $$->lineno = yylineno;
                }
                | MulExpression MODULO PostExpression {
                    $$ = new ArithmeticExpression($1, $3, ArithmeticExpression::Operation::MODULO);
                    $$->lineno = yylineno;
                };
PostExpression  : PrimExpression {
                    $$ = $1;
                }
                | Identifier LEFT_PAREN ArgExprList RIGHT_PAREN {
                    $$ = new CallExpression($1, $3);
                    $$->lineno = yylineno;
                }
                | Identifier LEFT_PAREN RIGHT_PAREN {
                    $$ = new CallExpression($1, NULL);
                    $$->lineno = yylineno;
                }
                ;
ArgExprList     : AssiExpression {
                    $$ = new std::vector<Expression*>();
                    $$->push_back($1);
                }
                | ArgExprList COMMA AssiExpression {
                    $$ = $1;
                    $$->push_back($3);
                };

IfStatement     : ALGO_IF CondExpression ALGO_THEN NewLine StatementList ALGO_ELSE NewLine StatementList ALGO_ENDIF NewLine {
                    $$ = new IfStatement($2, $5, $8);
                    $$->lineno = yylineno;
                }
                | ALGO_IF CondExpression ALGO_THEN NewLine StatementList ALGO_ENDIF NewLine {
                    $$ = new IfStatement($2, $5, NULL);
                    $$->lineno = yylineno;
                };

WhileEntireStart: ALGO_WHILE0 ALGO_WHILE1
                | ALGO_WHILE_SHORT;
ForEntireStep   : ALGO_STEP0 ALGO_STEP1
                | ALGO_STEP_SHORT;

LoopStatement   : WhileEntireStart CondExpression ALGO_DO NewLine StatementList ALGO_ENDWHILE NewLine {
                    $$ = new WhileStatement($2, $5);
                    $$->lineno = yylineno;
                }
                | ALGO_FOR Identifier ALGO_OF CondExpression ALGO_TO CondExpression ForEntireStep ALGO_OF CondExpression ALGO_DO NewLine StatementList ALGO_ENDFOR NewLine {
                    $$ = new ForLoopStatement($2, $4, $6, $9, $12);
                    $$->lineno = yylineno;
                } 
                | ALGO_FOR Identifier ALGO_OF CondExpression ALGO_TO CondExpression ALGO_DO NewLine StatementList ALGO_ENDFOR NewLine {
                    $$ = new ForLoopStatement($2, $4, $6, NULL, $9);
                    $$->lineno = yylineno;
                };

%%

int yywrap() {
   return 1;
}