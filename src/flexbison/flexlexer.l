%{
#include "bisonparser.hpp"

#include "../compiler.hpp"

void yyerror (char const *s);
extern Compiler* currentParser;

%}

%option nounput
%option noinput
%option noyywrap
%option yylineno

whitespace [ \t]+
newline [\r\n]+

%x S_SINGLELINE_COMMENT
%x S_STRING_LITERAL

%x S_ALGO_ROLE_START
%x S_ALGO_ROLE_CUR

%x ERROR_STATE

identifierBegin [A-z]
identifierBody [A-z_0-9]*

%%

\# {
    BEGIN(S_SINGLELINE_COMMENT);
}
<S_SINGLELINE_COMMENT>[^\n]+ { }
<S_SINGLELINE_COMMENT>\n { 
    BEGIN(INITIAL);
    return NEW_LINE;
}

"\"" {
    BEGIN(S_STRING_LITERAL);
}
<S_STRING_LITERAL>([^\\\"]|\\.)*\" { 
    BEGIN(INITIAL);
    yylval.strVal = strndup(yytext, strlen(yytext)-1);
    return STRING;
}


{identifierBegin}{identifierBody} {
    int keyword = currentParser->getKeywordFromCString(yytext);
    switch(keyword) {
    case ALGO_ROLE:
        BEGIN(S_ALGO_ROLE_START);
        return keyword;
    }
    if(keyword == IDENTIFIER) {
        yylval.strVal = strdup(yytext);
        return keyword;
    }
    return keyword;
}

<S_ALGO_ROLE_START>{whitespace} { }
<S_ALGO_ROLE_START>"\:" {
    BEGIN(S_ALGO_ROLE_CUR);
    return COLON;
}
<S_ALGO_ROLE_START>[^\:] {
    yyerror("Unexpected character at role start");
    return LEX_ERROR;
}

<S_ALGO_ROLE_CUR>[^\n]+ {
    yylval.strVal = strdup(yytext);
    return ALGO_ROLE_CONTENT;
}
<S_ALGO_ROLE_CUR>\n {
    BEGIN(INITIAL);
    return NEW_LINE;
}

{whitespace} {

}

{newline} { return NEW_LINE; }

[0-9]+/[^\.] {
    yylval.intVal = atoi(yytext);
    return NUMBER;
}

[0-9]+\.[0-9]* {
    yylval.realVal = atof(yytext);
    return FLOATNUMBER;
}

\: { return COLON; }
\, { return COMMA; }

=/[^=] {
    return ASSIGNMENT_OP;
}
\+ { return PLUS; }
\- { return MINUS; }
\* { return TIMES; }
\/ { return DIVIDE; }
\% { return MODULO; }

[\=\<\>\!]\= {
    switch(yytext[0]) {
    case '=':
        yylval.intVal = LogicalExpression::Operation::EQ;
        break;
    case '!':
        yylval.intVal = LogicalExpression::Operation::NEQ;
        break;
    case '<':
        yylval.intVal = LogicalExpression::Operation::LEQ;
        break;
    case '>':
        yylval.intVal = LogicalExpression::Operation::GEQ;
        break;
    }
    return CONDITIONAL_OP;
}
\>/[^=] {
    yylval.intVal = LogicalExpression::Operation::GREAT;
    return CONDITIONAL_OP;
}
\</[^=] {
    yylval.intVal = LogicalExpression::Operation::LESS;
    return CONDITIONAL_OP;
}

\( { return LEFT_PAREN; }
\) { return RIGHT_PAREN; }

\'.\' {
    yylval.chrVal = yytext[0];
    return CHARACTER;
}

. { fprintf(stderr, "Unexpected character '%s' at line %d\n", yytext, yylineno); BEGIN(ERROR_STATE); }

<<EOF>> { return END; }

%%

void yyerror (char const *s) {
    BEGIN(ERROR_STATE);
    fprintf (stderr, "%s\n", s);
}