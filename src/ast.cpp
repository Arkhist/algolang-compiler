#include "ast.hpp"

#include <iostream>

#include "compiler.hpp"
#include "types.hpp"

extern Compiler* currentParser;

AlgorithmAST::AlgorithmAST(std::string name, std::string description, VariableList* varlist, StatementList* statements) {
    this->name = name;
    this->description = description;
    this->variables = varlist;
    this->statements = statements;
}

bool AlgorithmAST::semanticCheck() {
    return this->statements == nullptr || this->statements->check(this->variables);
}

void AlgorithmAST::optimize() {
    this->variables->optimizeVariables();
}

void AlgorithmAST::compile(std::ofstream& outputFile) {
    outputFile << "/* " << this->description << " */" << std::endl;
    outputFile << "void " << this->name << "() {" << std::endl;
    this->variables->writeVariables(outputFile, 1);
    outputFile << std::endl;
    this->statements->write(outputFile, 1);
    outputFile << "}\n" << std::endl;
}

bool VariableList::appendVariables(std::vector<std::string>* idList, std::string typeIdentifier) {
    auto typeEntry = currentParser->getTypeFromString(typeIdentifier);
    if(typeEntry == VariableType::UNKNOWN) {
        std::cerr << "Type identifier " << typeIdentifier << " is invalid." << std::endl;
        return false;
    }
    VariableType type = typeEntry;
    for(const auto &identifier : *idList) {
        auto entry = this->variables.find(identifier);
        if(entry != this->variables.end()) {
            std::cerr << "Identifier " << identifier << " was already declared." << std::endl;
            return false;
        }
        this->variables[identifier] = type;
        this->untouchedVars.emplace(identifier);
    }
    return true;
}

VariableType VariableList::getVariableType(std::string varName) {
    auto typeEntry = this->variables.find(varName);
    if(typeEntry == this->variables.end())
        return VariableType::UNKNOWN;
    this->untouchedVars.erase(varName);
    return typeEntry->second;
}

void VariableList::optimizeVariables() {
    for(const auto &str : this->untouchedVars) {
        this->variables.erase(str);
        std::cerr << "Warning: Variable " << str << " is unused." << std::endl;
    }
    this->untouchedVars.clear();
}

void VariableList::writeVariables(std::ofstream& outputFile, int depth) {
    for(const auto &var : this->variables)
        outputFile << std::string(depth, '\t') <<  C_TYPE_MAP[(int)var.second] << " " << var.first << ";" << std::endl;
}