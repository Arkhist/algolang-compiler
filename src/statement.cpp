#include "statement.hpp"

#include <cassert>
#include <iostream>

#include "ast.hpp"

bool Statement::check(VariableList* variables) {
    std::cerr << "Line " << this->lineno << ": " << "Internal Semantic Check Error : Unhandled statement" << std::endl;
    return false;
}

bool Expression::typeCheck(VariableList* variables) {
    std::cerr << "Line " << this->lineno << ": " << "Internal Semantic Check Error : Unhandled expression" << std::endl;
    return false;
}

void StatementList::appendStatement(Statement* statement) {
    assert(statement != NULL && "Statement was null");
    this->statements.push_back(statement);
}

bool StatementList::check(VariableList* variables) {
    for(const auto &statement : this->statements) {
        if(!statement->check(variables))
            return false;
    }
    return true;
}

void StatementList::write(std::ofstream& outputFile, int depth) {
    for(const auto &statement : this->statements) {
        outputFile << std::string(depth, '\t');
        statement->write(outputFile, depth);
        if(!dynamic_cast<IfStatement*>(statement) && !dynamic_cast<LoopStatement*>(statement))
            outputFile << ";";
        outputFile << std::endl;
    }
}

IfStatement::IfStatement(Expression* condition, StatementList* positive, StatementList* negative) {
    this->condition = condition;
    this->positiveStatements = positive;
    this->negativeStatements = negative;
}

bool IfStatement::check(VariableList* variables) {
    if(!this->condition->typeCheck(variables) && !canCast(this->condition->getType(), VariableType::BOOL)) {
        return false;
    }
    return this->positiveStatements->check(variables) 
        && (this->negativeStatements == nullptr || this->negativeStatements->check(variables));
}

void IfStatement::write(std::ofstream& outputFile, int depth) {
    outputFile << "if (";
    this->condition->write(outputFile, depth);
    outputFile << ") {" << std::endl;
    this->positiveStatements->write(outputFile, depth+1);
    outputFile << std::string(depth, '\t') << "}"; 
    if(this->negativeStatements != nullptr) {
        outputFile << "\n" << std::string(depth, '\t') << "else {" << std::endl;
        this->negativeStatements->write(outputFile, depth+1);
        outputFile << std::string(depth, '\t') << "}";
    }
}

bool ForLoopStatement::check(VariableList* variables) {
    if(variables->getVariableType(this->loopIndexId) != VariableType::INT) {
        std::cerr << "Line " << this->lineno << ": " << "For Loop Identifier " + this->loopIndexId + " is not of type INT" << std::endl;
        return false;
    }
    if(!this->start->typeCheck(variables) || !canCast(this->start->getType(), VariableType::INT)) {
        std::cerr << "Line " << this->lineno << ": " << "For loop start expression is not of type INT" << std::endl;
        return false;
    }
    if(!this->start->typeCheck(variables) || !canCast(this->start->getType(), VariableType::INT)) {
        std::cerr << "Line " << this->lineno << ": " << "For loop end expression is not of type INT" << std::endl;
        return false;
    }
    if(this->step != nullptr && (!this->step->typeCheck(variables) || !canCast(this->step->getType(), VariableType::INT))) {
        std::cerr << "Line " << this->lineno << ": " << "For loop step expression is not of type INT" << std::endl;
        return false;
    }
    return this->loopBody->check(variables);
}

void ForLoopStatement::write(std::ofstream& outputFile, int depth) {
    outputFile << "for (" << this->loopIndexId << " = ";
    this->start->write(outputFile, depth);
    outputFile << "; " << this->loopIndexId << " <= ";
    this->end->write(outputFile, depth);
    outputFile << "; " << this->loopIndexId << " += ";
    if(this->step == nullptr)
        outputFile << "1";
    else
        this->step->write(outputFile, depth);
    outputFile << ") {" << std::endl;
    this->loopBody->write(outputFile, depth+1);
    outputFile << std::string(depth, '\t') << "}";
}

bool WhileStatement::check(VariableList* variables) {
    if(this->condition->typeCheck(variables) && canCast(this->condition->getType(), VariableType::BOOL))
        return this->loopBody->check(variables);
    std::cerr << "Line " << this->lineno << ": " << "While Loop condition is not of type BOOL" << std::endl;
    return false;
}
void WhileStatement::write(std::ofstream& outputFile, int depth) {
    outputFile << "while (";
    this->condition->write(outputFile, depth);
    outputFile << ") {" << std::endl;
    this->loopBody->write(outputFile, depth+1);
    outputFile << std::string(depth, '\t') << "}";
}


ConstantExpression::ConstantExpression(int val) {
    this->value = val;
    this->returnType = VariableType::INT;
}
ConstantExpression::ConstantExpression(double val) {
    this->value = val;
    this->returnType = VariableType::REAL;
}
ConstantExpression::ConstantExpression(char val) {
    this->value = val;
    this->returnType = VariableType::CHARACTER;
}
ConstantExpression::ConstantExpression(std::string str, bool isIdentifier) {
    this->value = str;
    this->returnType = isIdentifier ? VariableType::IDENTIFIER_TYPE : VariableType::STRING;
}
ConstantExpression::ConstantExpression(bool val) {
    this->value = val;
    this->returnType = VariableType::BOOL;
}
bool ConstantExpression::typeCheck(VariableList* variables) {
    if(this->returnType == VariableType::IDENTIFIER_TYPE) {
        auto type = variables->getVariableType(std::get<std::string>(this->value));
        if(type == VariableType::UNKNOWN)
            return false;
        this->identifierType = type;
    }
    return true;
}
VariableType ConstantExpression::getType() {
    if(this->returnType != VariableType::IDENTIFIER_TYPE)
        return this->returnType;
    return this->identifierType;
}
void ConstantExpression::write(std::ofstream& outputFile, int depth) {
    switch(this->returnType) {
    case VariableType::INT:
        outputFile << std::get<int>(this->value);
        break;
    case VariableType::REAL:
        outputFile << std::get<double>(this->value);
        break;
    case VariableType::STRING:
        outputFile << "\"" << std::get<std::string>(this->value) << "\"";
        break;
    case VariableType::IDENTIFIER_TYPE:
        outputFile << std::get<std::string>(this->value);
        break;
    case VariableType::BOOL:
        outputFile << (std::get<bool>(this->value) ? "true" : "false");
        break;
    default:
        outputFile << "/* UNHANDLED CONSTANT EXPR */";
    }
}

AssignmentExpression::AssignmentExpression(std::string id, Expression* expr) {
    this->identifier = id;
    this->rVal = expr;
}
bool AssignmentExpression::typeCheck(VariableList* variables) {
    VariableType target = variables->getVariableType(this->identifier);
    if(target == VariableType::UNKNOWN) {
        std::cerr << "Line " << this->lineno << ": " << "Unknown variable identifier " << this->identifier << std::endl;
        return false;
    }
    this->returnType = target;
    if(!this->rVal->typeCheck(variables) || !canCast(this->rVal->getType(), target)) {
        std::cerr << "Line " << this->lineno << ": " << "Impossible to cast assignment right value to desired type" << std::endl;
        return false;
    }
    return true;
}
void AssignmentExpression::write(std::ofstream& outputFile, int depth) {
    outputFile << this->identifier << " = ";
    this->rVal->write(outputFile, depth);
}

CallExpression::CallExpression(std::string id, std::vector<Expression*>* args) {
    this->identifier = id;
    this->arguments = args;
}
bool CallExpression::typeCheck(VariableList* variables) {
    if(this->identifier == "afficher" || this->identifier == "print") {
        for(const auto &expr : *this->arguments) {
            if(!expr->typeCheck(variables)) {
                std::cerr << "Line " << this->lineno << ": " << "Invalid argument for print instruction" << std::endl;
                return false;
            }
        }
    }
    else if(this->identifier == "lire" || this->identifier == "read") {
        for(const auto &expr : *this->arguments) {
            if(!expr->typeCheck(variables)) {
                std::cerr << "Line " << this->lineno << ": " << "Invalid argument for read instruction" << std::endl;
                return false;
            }
            auto cstExpr = dynamic_cast<ConstantExpression*>(expr);
            if(!cstExpr || !cstExpr->isIdentifier()) {
                std::cerr << "Line " << this->lineno << ": " << "Non writable argument for read instruction" << std::endl;
                return false;
            }
        }
    }
    return true;
}
void CallExpression::write(std::ofstream& outputFile, int depth) {
    if(this->identifier == "afficher" || this->identifier == "print") {
        outputFile << "printf(\"";
        for(const auto &expr : *this->arguments) {
            switch(expr->getType()) {
            case VariableType::INT:
                outputFile << "%d";
                break;
            case VariableType::REAL:
                outputFile << "%lf";
                break;
            case VariableType::CHARACTER:
                outputFile << "%c";
                break;
            case VariableType::STRING:
                outputFile << "%s";
                break;
            default:
                outputFile << "??";
                break;
            }
        }
        outputFile << "\"";
        for(const auto &expr : *this->arguments) {
            outputFile << ", ";
            expr->write(outputFile, depth);
        }
        outputFile << ")";
        return;
    }
    else if(this->identifier == "lire" || this->identifier == "read") {
        outputFile << "scanf(\"";
        for(const auto &expr : *this->arguments) {
            switch(expr->getType()) {
            case VariableType::BOOL:
            case VariableType::INT:
                outputFile << "%d";
                break;
            case VariableType::REAL:
                outputFile << "%lf";
                break;
            case VariableType::CHARACTER:
                outputFile << "%c";
                break;
            case VariableType::STRING:
                outputFile << "%s";
                break;
            default:
                outputFile << "??";
                break;
            }
            if(expr != this->arguments->back()) {
                outputFile << " ";
            }
        }

        outputFile << "\"";
        for(const auto &expr : *this->arguments) {
            outputFile << ", &";
            expr->write(outputFile, depth);
        }
        outputFile << ")";
        return;
    }
    outputFile << "/* UNHANDLED CALL EXPRESSION */";
}

LoopStatement::LoopStatement(StatementList* body) {
    this->loopBody = body;
}

ForLoopStatement::ForLoopStatement(std::string id, Expression* start, Expression* end, Expression* step, StatementList* body) : LoopStatement(body) {
    this->loopIndexId = id;
    this->start = start;
    this->end = end;
    this->step = step;
}

WhileStatement::WhileStatement(Expression* condition, StatementList* body) : LoopStatement(body) {
    this->condition = condition;
}

BinaryExpression::BinaryExpression(Expression* lVal, Expression* rVal) {
    this->lVal = lVal;
    this->rVal = rVal;
}

LogicalExpression::LogicalExpression(Expression* lVal, Expression* rVal, Operation op) : BinaryExpression(lVal, rVal) {
    this->op = op;
    this->returnType = VariableType::BOOL;
}
bool LogicalExpression::typeCheck(VariableList* variables) {
    if(!this->lVal->check(variables)) {
        std::cerr << "Line " << this->lineno << ": " << "Logical Expression left value is invalid" << std::endl;
        return false;
    }
    if(!this->rVal->check(variables)) {
        std::cerr << "Line " << this->lineno << ": " << "Logical Expression right value is invalid" << std::endl;
        return false;
    }
    
    if(this->op & 0b10000) // Logical Operation
        return canCast(this->lVal->getType(), VariableType::BOOL) && 
            canCast(this->rVal->getType(), VariableType::BOOL);
    else if(this->op & 0b00100) { // Number comparison
        if(!canCast(this->lVal->getType(), VariableType::INT)) {
            std::cerr << "Line " << this->lineno << ": " << "Logical expression left value can't be number-compared" << std::endl;
            return false;
        }
        if(!canCast(this->rVal->getType(), VariableType::INT)) {
            std::cerr << "Line " << this->lineno << ": " << "Logical expression right value can't be number-compared" << std::endl;
            return false;
        }
        return true;
    }
    else {
        if(!canCast(this->lVal->getType(), this->rVal->getType())) {
            std::cerr << "Line " << this->lineno << ": " << "Logical expression left and right value are not comparable" << std::endl;
        }
        return true;
    }
}
void LogicalExpression::write(std::ofstream& outputFile, int depth) {
    if(this->op == Operation::NOT) {
        outputFile << "!(";
        this->lVal->write(outputFile, depth);
        outputFile << ")";
        return;
    }
    outputFile << "(";
    this->lVal->write(outputFile, depth);
    switch(this->op) {
    case Operation::AND:
        outputFile << " && ";
        break;
    case Operation::OR:
        outputFile << " || ";
        break;
    case Operation::LEQ:
        outputFile << " <= ";
        break;
    case Operation::LESS:
        outputFile << " < ";
        break;
    case Operation::GEQ:
        outputFile << " >= ";
        break;
    case Operation::GREAT:
        outputFile << " > ";
        break;
    case Operation::EQ:
        outputFile << " == ";
        break;
    case Operation::NEQ:
        outputFile << " != ";
        break;
    default:
        outputFile << "/* UNHANDLED LOGICAL EXPRESSION */";
    }
    this->rVal->write(outputFile, depth);
    outputFile << ")";
}

ArithmeticExpression::ArithmeticExpression(Expression* lVal, Expression* rVal, Operation op) : BinaryExpression(lVal, rVal) {
    this->op = op;
}
bool ArithmeticExpression::typeCheck(VariableList* variables) {
    if(!this->lVal->typeCheck(variables) || !canCast(this->lVal->getType(), VariableType::INT)) {
        std::cerr << "Line " << this->lineno << ": " << "Arithmetic expression left value is invalid" << std::endl;
        return false;
    }
    if(!this->rVal->typeCheck(variables) || !canCast(this->rVal->getType(), VariableType::INT)) {
        std::cerr << "Line " << this->lineno << ": " << "Arithmetic expression right value is invalid" << std::endl;
        return false;
    }
    if(this->op == Operation::MODULO) {
        this->returnType = VariableType::INT;
        if(this->lVal->getType() != VariableType::INT || this->rVal->getType() != VariableType::INT) {
            std::cerr << "Line " << this->lineno << ": " << "Modulo only supports integers" << std::endl;
            return false;
        }
    }
    else {
        if(this->lVal->getType() == VariableType::REAL || this->rVal->getType() == VariableType::REAL)
            this->returnType = VariableType::REAL;
        else
            this->returnType = VariableType::INT;
    }
    return true;
}
void ArithmeticExpression::write(std::ofstream& outputFile, int depth) {
    outputFile << "(";
    this->lVal->write(outputFile, depth);
    switch(this->op) {
    case Operation::PLUS:
        outputFile << " + ";
        break;
    case Operation::MINUS:
        outputFile << " - ";
        break;
    case Operation::TIMES:
        outputFile << " * ";
        break;
    case Operation::DIVIDE:
        outputFile << " / ";
        break;
    case Operation::MODULO:
        outputFile << " % ";
        break;
    default:
        outputFile << "/* UNHANDLED ARITHMETIC EXPRESSION */";
    }
    this->rVal->write(outputFile, depth);
    outputFile << ")";
}