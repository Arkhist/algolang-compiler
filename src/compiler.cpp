#include "compiler.hpp"

#include <iostream>
#include <fstream>

#include "flexbison/flexlexer.hpp"
#include "flexbison/bisonparser.hpp"

extern int yydebug;
extern Compiler* currentParser;

Compiler::Compiler() {

}

ParseResult Compiler::parse(const char* filename) {
    FILE* inputFile;
    inputFile = fopen(filename, "r");
    if(inputFile == NULL) {
        std::cerr << "Impossible to open file " << filename << std::endl;
        return PARSE_FAILURE;
    }
#ifdef ARKDEBUG
    //yydebug = 1;
#endif
    yyrestart(inputFile);
    currentParser = this;

    if(this->language == nullptr) {
        if(!this->setLanguage("fr")) {
            return INTERNAL_ERROR;
        }
    }

    yyparse();
    for(const auto &algorithm : this->ASTs) {
        if(algorithm.second->semanticCheck()) {
            std::cout << algorithm.first << " is valid" << std::endl;
            algorithm.second->optimize();
        }
        else {
            std::cout << algorithm.first << " is invalid" << std::endl;
            return PARSE_FAILURE;
        }
    }

    return PARSE_SUCCESS;
}

bool Compiler::addAlgorithm(AlgorithmAST* ast) {
    if(ast->name == "") {
        int i = 1;
        std::string candidate = "algo" + std::to_string(i) + "a";
        while(this->ASTs.find(candidate) != this->ASTs.end())
            candidate = "algo" + std::to_string(++i) + "a";
        ast->name = candidate;
    }
    else if(this->ASTs.find(ast->name) != this->ASTs.end()) {
        std::cerr << "Algorithm named " << ast->name << " already declared before." << std::endl;
        return false;
    }
    this->ASTs[ast->name] = ast;
    return true;
}

void Compiler::compile(std::string filename) {
    std::ofstream outputFile;
    outputFile.open(filename == "" ? "algo.c" : filename, std::ofstream::out);
    outputFile << "#include <stdlib.h>\n#include <stdio.h>\ntypedef enum { false = 0, true = 1 } _bool;\n" << std::endl;
    for(const auto &algorithm : this->ASTs) {
        algorithm.second->compile(outputFile);
    }
}

int Compiler::getKeywordFromCString(char* cstr) {
    auto entry = this->language->keywords->find(cstr);
    if(entry == this->language->keywords->end()) {
        return IDENTIFIER;
    }
    return entry->second;
}

VariableType Compiler::getTypeFromString(std::string string) {
    auto typeEntry = this->language->types->find(string);
    if(typeEntry == this->language->types->end()) {
        return VariableType::UNKNOWN;
    }
    return (VariableType)typeEntry->second;
}

void Compiler::setVerboseLevel(int level) {
    this->verboseLevel = level;
}

bool Compiler::setLanguage(std::string lang) {
    auto langEntry = languages.find(lang);
    if(langEntry == languages.end()) {
        return false;
    }
    this->language = langEntry->second;
    return true;
}