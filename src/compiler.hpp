#pragma once

#include <string>
#include <unordered_map>

#include "language.hpp"
#include "ast.hpp"

enum ParseResult {
    PARSE_SUCCESS,
    PARSE_FAILURE,
    INTERNAL_ERROR
};

class Compiler
{
public:
    Compiler();

    ParseResult parse(const char* filename);
    bool addAlgorithm(AlgorithmAST* ast);

    void compile(std::string filename);

    void setVerboseLevel(int level);
    bool setLanguage(std::string lang);

    int getKeywordFromCString(char* string);
    VariableType getTypeFromString(std::string string);
private:
    std::unordered_map<std::string, AlgorithmAST*> ASTs;

    LanguageDef const * language = nullptr;

    int verboseLevel = 0;
};