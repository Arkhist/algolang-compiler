#include "language.hpp"

#include "flexbison/bisonparser.hpp"

#pragma region FR
    static std::unordered_map<std::string, int> const keywords_fr = {
        { "Algorithme", ALGO_DECL },
        { "Role", ALGO_ROLE },
        { "Declaration", ALGO_VAR },
        { "Debut", ALGO_BEGIN },
        { "Fin", ALGO_END },

        { "si", ALGO_IF },
        { "sinon", ALGO_ELSE },
        { "alors", ALGO_THEN },
        { "fsi", ALGO_ENDIF },

        { "et", AND },
        { "ou", OR },
        { "non", NOT },

        { "pour", ALGO_FOR },
        { "de", ALGO_OF },
        { "a", ALGO_TO },
        { "par", ALGO_STEP0 },
        { "pas", ALGO_STEP1 },
        { "faire", ALGO_DO },
        { "fpour", ALGO_ENDFOR },

        { "tant", ALGO_WHILE0 },
        { "que", ALGO_WHILE1 },
        { "ftant", ALGO_ENDWHILE },

        { "vrai", ALGO_TRUE },
        { "faux", ALGO_FALSE }
    };

    static std::unordered_map<std::string, VariableType> const types_fr = {
        { "entier", VariableType::INT },
        { "reel", VariableType::REAL },
        { "chaine", VariableType::STRING },
        { "caractere", VariableType::CHARACTER },
        { "booleen", VariableType::BOOL }
    };

    static LanguageDef const lang_fr = (LanguageDef){&keywords_fr, &types_fr};
#pragma endregion FR

#pragma region EN
    static std::unordered_map<std::string, int> const keywords_en = {
        { "Algorithm", ALGO_DECL },
        { "Role", ALGO_ROLE },
        { "Declaration", ALGO_VAR },
        { "Begin", ALGO_BEGIN },
        { "End", ALGO_END },

        { "if", ALGO_IF },
        { "else", ALGO_ELSE },
        { "then", ALGO_THEN },
        { "endif", ALGO_ENDIF },

        { "and", AND },
        { "or", OR },
        { "not", NOT },

        { "for", ALGO_FOR },
        { "from", ALGO_OF },
        { "to", ALGO_TO },
        { "step", ALGO_STEP_SHORT },
        { "do", ALGO_DO },
        { "endfor", ALGO_ENDFOR },

        { "while", ALGO_WHILE_SHORT },
        { "endwhile", ALGO_ENDWHILE },

        { "true", ALGO_TRUE },
        { "false", ALGO_FALSE }
    };

    static std::unordered_map<std::string, VariableType> const types_en = {
        { "integer", VariableType::INT },
        { "real", VariableType::REAL },
        { "string", VariableType::STRING },
        { "character", VariableType::CHARACTER },
        { "boolean", VariableType::BOOL }
    };

    static LanguageDef const lang_en = (LanguageDef){&keywords_en, &types_en};
#pragma endregion EN

std::unordered_map<std::string, LanguageDef const *> languages = {
    {"fr", &lang_fr},
    {"en", &lang_en}
};