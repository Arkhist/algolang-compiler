#pragma once

#include <unordered_map>

#include "types.hpp"

struct LanguageDef {
    std::unordered_map<std::string, int> const * const keywords;
    std::unordered_map<std::string, VariableType> const * const types;
};

extern std::unordered_map<std::string, LanguageDef const *> languages;