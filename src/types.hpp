#pragma once

#include <variant>
#include <string>

enum class VariableType {
    INT = 0,
    REAL = 1,
    STRING = 2,
    CHARACTER = 3,
    BOOL = 4,
    UNKNOWN = 5,
    IDENTIFIER_TYPE
};

const std::string C_TYPE_MAP[6] = {
    "int",
    "double",
    "char*",
    "char",
    "_bool",
    "void"
};

const bool TYPE_CAST_MATRIX[6][6] = {
    {true, true, false, false, false, false},
    {true, true, false, false, false, false},
    {false, false, true, false, false, false},
    {false, false, false, true, false, false},
    {false, false, false, false, true, false},
    {false, false, false, false, false, false}
};

bool canCast(VariableType from, VariableType to);