#include "types.hpp"

bool canCast(VariableType from, VariableType to) {
    if(from == VariableType::IDENTIFIER_TYPE || to == VariableType::IDENTIFIER_TYPE)
        return false;
    return TYPE_CAST_MATRIX[(int)from][(int)to];
}