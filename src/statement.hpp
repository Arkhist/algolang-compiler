#pragma once

#include <vector>
#include <string>
#include <fstream>

#include "types.hpp"

class VariableList;

class Statement {
public:
    virtual bool check(VariableList* variables);

    virtual void write(std::ofstream& outputFile, int depth) { outputFile << "// INVALID STATEMENT"; }

    int lineno = -1;
};

class StatementList {
public:
    void appendStatement(Statement* statement);

    bool check(VariableList* variables);

    void write(std::ofstream& outputFile, int depth);
private:
    std::vector<Statement*> statements;
};

class Expression : public Statement {
public:
    virtual bool check(VariableList* variables) { return typeCheck(variables); }
    virtual bool typeCheck(VariableList* variables);

    virtual VariableType getType() { return this->returnType; }
    virtual void write(std::ofstream& outputFile, int depth) { outputFile << "/* INVALID EXPRESSION */"; }
protected:
    VariableType returnType = VariableType::UNKNOWN;
};

class IfStatement : public Statement {
public:
    IfStatement(Expression* condition, StatementList* positive, StatementList* negative);

    virtual bool check(VariableList* variables);

    virtual void write(std::ofstream& outputFile, int depth);
private:
    Expression* condition;
    StatementList* positiveStatements;
    StatementList* negativeStatements;
};

class LoopStatement : public Statement {
public:
    LoopStatement(StatementList* body);
protected:
    StatementList* loopBody;
};

class ForLoopStatement : public LoopStatement {
public:
    ForLoopStatement(std::string id, Expression* start, Expression* end, Expression* step, StatementList* body);

    virtual bool check(VariableList* variables);

    virtual void write(std::ofstream& outputFile, int depth);
private:
    std::string loopIndexId;
    Expression* start;
    Expression* end;
    Expression* step;
};

class WhileStatement : public LoopStatement {
public:
    WhileStatement(Expression* condition, StatementList* body);

    virtual bool check(VariableList* variables);

    virtual void write(std::ofstream& outputFile, int depth);
private:
    Expression* condition;
};

class AssignmentExpression : public Expression {
public:
    AssignmentExpression(std::string id, Expression* expr);

    virtual bool typeCheck(VariableList* variables);

    virtual void write(std::ofstream& outputFile, int depth);
private:
    std::string identifier;
    Expression* rVal;
};

class BinaryExpression : public Expression {
public:
    BinaryExpression(Expression* lVal, Expression* rVal);
protected:
    Expression* lVal;
    Expression* rVal;
};

class ArithmeticExpression : public BinaryExpression {
public:
    enum Operation {
        PLUS,
        MINUS,
        TIMES,
        DIVIDE,
        MODULO
    };

    ArithmeticExpression(Expression* lVal, Expression* rVal, Operation op);

    bool typeCheck(VariableList* variables);

    virtual void write(std::ofstream& outputFile, int depth);
private:
    Operation op;
};

class LogicalExpression : public BinaryExpression {
public:
    enum Operation {
        AND =   0b10000,
        OR =    0b10001,
        NOT =   0b10010,

        EQ =    0b01000,
        NEQ =   0b01001,

        LESS =  0b00100,
        LEQ =   0b00101,
        GREAT = 0b00110,
        GEQ =   0b00111
    };

    LogicalExpression(Expression* lVal, Expression* rVal, Operation op);

    bool typeCheck(VariableList* variables);

    virtual void write(std::ofstream& outputFile, int depth);
private:
    Operation op;
};

class ConstantExpression : public Expression {
public:
    ConstantExpression(int val);
    ConstantExpression(double val);
    ConstantExpression(char val);
    ConstantExpression(std::string str, bool isIdentifier);
    ConstantExpression(bool val);

    virtual bool typeCheck(VariableList* variables);
    virtual VariableType getType();
    bool isIdentifier() { return identifierType != VariableType::UNKNOWN; }
    std::string getIdentifier() { return std::get<std::string>(this->value); }

    virtual void write(std::ofstream& outputFile, int depth);
private:
    std::variant<int, std::string, double, char, bool> value;
    VariableType identifierType = VariableType::UNKNOWN;
};

class CallExpression : public Expression {
public:
    CallExpression(std::string id, std::vector<Expression*>* args);

    bool typeCheck(VariableList* variables);

    virtual void write(std::ofstream& outputFile, int depth);
private:
    std::string identifier;
    std::vector<Expression*>* arguments;
};