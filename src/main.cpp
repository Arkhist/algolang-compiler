#include <iostream>

#include "popl/popl.hpp"

#include "compiler.hpp"

int main(int argc, char** argv)
{
    popl::OptionParser options("Allowed options");
    auto help_option = options.add<popl::Switch>("h", "help", "Show help for algocomp");
    auto outputFile_option = options.add<popl::Value<std::string>>("o", "output", "Set output file of compiler", "output.c");

    auto languageList_option = options.add<popl::Switch>("", "langlist", "Show all languages available");
    auto language_option = options.add<popl::Value<std::string>>("", "lang", "Set language of the algolang", "fr");

    options.parse(argc, argv);

    if(help_option->count() >= 1) {
        std::cout << options << std::endl;
        return 0;
    }
    if(languageList_option->count() >= 1) {
        std::cout << "Available Algolang Languages: " << std::endl;
        for(auto entry : languages) {
            std::cout << "\t- " << entry.first << std::endl;
        }
        return 0;
    }

    if(options.non_option_args().size() == 0) {
        std::cerr << argv[0] << ": Fatal error: no files to compile." << std::endl;
        return 1;
    }
    if(options.non_option_args().size() > 1) {
        std::cerr << argv[0] << ": Error: algocomp can only process one file at a time." << std::endl;
        return 2;
    }
    
    auto compiler = Compiler();
    if(!compiler.setLanguage(language_option->value())) {
        std::cerr << argv[0] << ": Error: Language " << language_option->value() << " not available." << std::endl;
        return 3;
    }
    auto result = compiler.parse(options.non_option_args().front().c_str());
    if(result == PARSE_FAILURE) {
        return 4;
    }

    compiler.compile(outputFile_option->value());

    return 0;
}