#pragma once

#include <string>
#include <unordered_map>
#include <vector>
#include <set>

#include <fstream>

#include "types.hpp"
#include "statement.hpp"

class VariableList {
public:
    bool appendVariables(std::vector<std::string>* idList, std::string typeIdentifier);

    VariableType getVariableType(std::string varName);

    void optimizeVariables();
    void writeVariables(std::ofstream& outputFile, int depth);
private:
    std::unordered_map<std::string, VariableType> variables;
    std::set<std::string> untouchedVars;
};

class AlgorithmAST {
public:
    AlgorithmAST(std::string name, std::string description, VariableList* varlist, StatementList* statements);
    bool semanticCheck();
    void optimize();

    void compile(std::ofstream& outputFile);

    std::string name;
private:
    std::string description;
    VariableList* variables;
    StatementList* statements;
};