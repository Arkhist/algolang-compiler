# The Ark C/CPP Makefile:tm: 29/04/2020
NAME= algocomp

CC= clang
CFLAGS= --std=c11 -Wall -Wextra -D_XOPEN_SOURCE=700
CXX= clang++
CPPFLAGS= --std=c++17 -Wall -Wextra -D_XOPEN_SOURCE=700 -Wno-unused-parameter
LINKER=$(CXX)

LEXGEN=flex
PARGEN=bison

LDFLAGS= 

SRCDIR= src
OBJDIR= obj
#===== END CONFIG =====#

rwildcard=$(foreach d,$(wildcard $(1:=/*)),$(call rwildcard,$d,$2) $(filter $(subst *,%,$2),$d))
SRC_C = $(call rwildcard,$(SRCDIR),*.c)
OBJ_C = $(SRC_C:$(SRCDIR)/%.c=$(OBJDIR)/%.o)
SRC_CPP = $(call rwildcard,$(SRCDIR),*.cpp)
OBJ_CPP = $(SRC_CPP:$(SRCDIR)/%.cpp=$(OBJDIR)/%.o)
OBJ= $(OBJDIR)/flexbison/bisonparser.o $(OBJDIR)/flexbison/flexlexer.o $(OBJ_C) $(OBJ_CPP) 

DEBUG = no
ifeq ($(DEBUG), yes)
  CFLAGS += -O0 -g -DARKDEBUG
  CPPFLAGS += -O0 -g -DARKDEBUG
else
  CFLAGS += -O3 -DNDEBUG
  CPPFLAGS += -O3 -DNDEBUG
endif
# AUTO VARIABLE DEFINITION


build: $(NAME)

$(NAME): $(OBJ)
	$(LINKER) -o $@ $^ $(LDFLAGS)

-include $(OBJ:.o=.d)

$(SRCDIR)/flexbison/bisonparser.cpp: $(SRCDIR)/flexbison/bisonparser.y
	$(PARGEN) $< --output=$(SRCDIR)/flexbison/bisonparser.cpp --defines=$(SRCDIR)/flexbison/bisonparser.hpp

$(SRCDIR)/flexbison/flexlexer.cpp: $(SRCDIR)/flexbison/flexlexer.l
	$(LEXGEN) --outfile=$(SRCDIR)/flexbison/flexlexer.cpp --header-file=$(SRCDIR)/flexbison/flexlexer.hpp -s $<

# Generic C rule
$(OBJDIR)/%.o: $(SRCDIR)/%.c
	@mkdir -p $(@D)
	$(CC) -o $@ $< $(CFLAGS) -c -MMD
	@mv -f $(OBJDIR)/$*.d $(OBJDIR)/$*.d.tmp
	@sed -e 's|.*:|$(OBJDIR)/$*.o:|' < $(OBJDIR)/$*.d.tmp > $(OBJDIR)/$*.d
	@sed -e 's/.*://' -e 's/\\$$//' < $(OBJDIR)/$*.d.tmp | fmt -1 | \
	  sed -e 's/^ *//' -e 's/$$/:/' >> $(OBJDIR)/$*.d
	@rm -f $(OBJDIR)/$*.d.tmp
	@sed -i '/\\\:/d' $(OBJDIR)/$*.d
# Generic CPP rule
$(OBJDIR)/%.o: $(SRCDIR)/%.cpp
	@mkdir -p $(@D)
	$(CXX) -o $@ $< $(CPPFLAGS) -c -MMD
	@mv -f $(OBJDIR)/$*.d $(OBJDIR)/$*.d.tmp
	@sed -e 's|.*:|$(OBJDIR)/$*.o:|' < $(OBJDIR)/$*.d.tmp > $(OBJDIR)/$*.d
	@sed -e 's/.*://' -e 's/\\$$//' < $(OBJDIR)/$*.d.tmp | fmt -1 | \
	  sed -e 's/^ *//' -e 's/$$/:/' >> $(OBJDIR)/$*.d
	@rm -f $(OBJDIR)/$*.d.tmp
	@sed -i '/\\\:/d' $(OBJDIR)/$*.d

.PHONY: clean
clean:
	rm -f $(NAME)
	rm -rf $(OBJDIR)/*
	rm -rf $(SRCDIR)/flexbison/bisonparser.cpp $(SRCDIR)/flexbison/bisonparser.hpp
	rm -rf $(SRCDIR)/flexbison/flexlexer.cpp $(SRCDIR)/flexbison/flexlexer.hpp