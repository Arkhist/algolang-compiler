# Algolang-Compiler  [![UPVD Logo](https://upload.wikimedia.org/wikipedia/fr/e/e6/UPVD_logo.svg)](https://www.univ-perp.fr)

> Compiler for simple Algorithmic Language

Algolang-Compiler (AC) is a compiler from a simple algorithmic language in French to the language C.

## Building

### Dependencies

The project requires `bison` and `flex` installed.

### Compiling

To build the project run the provided Makefile:

```shell
make
```

You can add to the following command lines the following options to change the resulting build:

- `DEBUG=yes` : Adds debug symbols to the project.

## Running

Run the `algocomp` executable.

Use the option `-h` to have information on all the options available.

Example: `algocomp -o output.c file.algo`

## Contact

You can contact me at:

Peyrille Benjamin (peyrille.benjamin@gmail.com)
